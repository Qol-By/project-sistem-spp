<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_laporan extends CI_Model {

    public function cariTransaksi($bulan, $tahun){
        $query = $this->db->query("SELECT tb_siswa.nis, tb_siswa.nama_siswa, tb_kelas.kode_kelas, (jumlah_bayar + jumlah_bayar_2)
 AS jumlah_bayar FROM tb_pembayaran INNER JOIN tb_siswa ON tb_pembayaran.id_siswa = tb_siswa.id_siswa INNER JOIN tb_kelas 
 ON tb_pembayaran.id_kelas = tb_kelas.id_kelas WHERE p_bulan = ".$bulan. " AND
  p_tahun = ".$tahun." ORDER BY tb_kelas.kode_kelas, tb_siswa.nis;");
        return $query->result();
    }

	public function cariTransaksiKelas($kelas, $bulan, $tahun){
		$query = $this->db->query("SELECT tb_siswa.nis, tb_siswa.nama_siswa, tb_kelas.kode_kelas, (jumlah_bayar + jumlah_bayar_2)
 AS jumlah_bayar FROM tb_pembayaran INNER JOIN tb_siswa ON tb_pembayaran.id_siswa = tb_siswa.id_siswa INNER JOIN tb_kelas 
 ON tb_pembayaran.id_kelas = tb_kelas.id_kelas WHERE p_bulan = ".$bulan. " AND
  p_tahun = ".$tahun." AND tb_kelas.id_kelas = " . $kelas. " ORDER BY tb_kelas.kode_kelas, tb_siswa.nis;");
		return $query->result();
	}

	public function cariTransaksiTahun($tahun){
		$query = $this->db->query("SELECT p_bulan, p_tahun, SUM(jumlah_bayar) AS jumlah_bayar, SUM(jumlah_bayar_2) AS jumlah_bayar_2  
FROM `tb_pembayaran` WHERE p_tahun = ".$tahun." GROUP BY p_bulan");
		return $query->result();
	}


}
